# FunKy CLI

## Install

Add the location of `funky` to your `PATH`, for example:

```shell
HOME/Dropbox/funky-cloud/funky-cli
```

Create a config file, for example:

> `funky.remote.config`
```shell
funky_store="http://192.168.64.116:8090/funktions";
funky_store_token="SUxPVkVQQU5EQVMK";
funky_regulator="http://192.168.64.116:8080/funk";
```

> - `192.168.64.116` is the ip of your funky server (you can replace it by a domain name)

Set the `FUNKY_CLOUD_CONFIG` with the path to the config file, for example:
```shell
export FUNKY_CLOUD_CONFIG=./funky.remote.config
```

## Use it

- Create a function (or funktion): `funky apply funktion_name source_code`
  - `funky apply hello-js index.js`
- Call the function: `funky call funktion_name json_parameter` 
  - `funky call hello-js '{"name":"k33g"}'`
- Get the list of the functions: `funky list` or `funky list json`
- Kill (stop) a function: `funky kill funktion_name`

or type `funky help`

## Write a function

> 🚧 WIP

See `/samples`

A funky-cloud **funKtion** is defined in a single file, with at least 2 functions `main` and `index`. 

- The `main` function is triggered when you use the command `funky call` (`POST`)
- The `index` function is triggered when you use the command `funky index` (`GET`)

```kotlin
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import io.vertx.ext.web.RoutingContext
import io.vertx.core.json.JsonObject

fun main(params: JsonObject, context: RoutingContext): Any {
  
  return json {
    obj(
      "message" to "Hello World!!!",
      "total" to 42,
      "name" to params.getString("name")
    )
  }.encodePrettily()
}

fun index(): String {
  return "<h1>🚀 Kotlin rocks</h1>"
}
```




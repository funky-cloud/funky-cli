$ brew install rustup
$ rustup-init
$ rustup default nightly
$ rustup target add wasm32-unknown-unknown

cargo install wasm-pack

cargo install wasm-gc

cargo install https

---

cargo new --lib utils

---

Add in Cargo.toml:
[lib]
crate-type = ["cdylib"]

Add in lib.rs:
#[no_mangle]
pub extern fn add_one(x: u32) -> u32 {
    x + 1
}


cd utils
cargo build --target wasm32-unknown-unknown --release
wasm-gc target/wasm32-unknown-unknown/release/utils.wasm -o utils.gc.wasm
#!/bin/bash

cargo build --target wasm32-unknown-unknown --release
wasm-gc target/wasm32-unknown-unknown/release/hello_rust.wasm -o hello_rust.gc.wasm
cp hello_rust.gc.wasm ../index.wasm

